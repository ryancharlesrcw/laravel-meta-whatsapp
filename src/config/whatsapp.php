<?php

return [
    "api_token" => env("WHATSAPP_API_TOKEN"),
    "phone_number_id" => env("WHATSAPP_PHONE_NUMBER_ID"),
];