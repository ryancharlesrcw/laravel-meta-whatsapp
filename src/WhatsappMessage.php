<?php

namespace RyanCharlesWijaya\LaravelMetaWhatsapp;

use Exception;
use Illuminate\Support\Facades\Http;

class WhatsappMessage {
    private $recipient_phone_number;
    
    public function to($phone_number)
    {
        $this->recipient_phone_number = $phone_number;

        return $this;
    }

    public function send($message)
    {
        $message_type = $this->checkMessageType($message);
        $headers = [
            "Authorization" => "Bearer ".config("whatsapp.api_token")
        ];

        Http::withHeaders($headers)
            ->post(
                "https://graph.facebook.com/v17.0/".config("whatsapp.phone_number_id")."/messages",
                [
                    "messaging_product" => "whatsapp",
                    "to" => $this->recipient_phone_number,
                    "type" => $message_type,
                    $message_type => [
                        ($message_type == "text") ? "body" : "link" => $message
                    ]
                ]
            );
    }

    private function checkMessageType($message) : string
    {
        if (is_string($message)) {
            return "text";
        } else if (file($message)) {
            $file_extension = pathinfo($message, PATHINFO_EXTENSION);

            $image_extensions = ["jpg", "png", "jpeg", "gif"];
            $video_extensions = ["mp4"];
            $audio_extensions = ["mp3"];

            if (in_array($file_extension, $image_extensions)) {
                return "image";
            } else if (in_array($file_extension, $video_extensions)) {
                return "video";
            } else if (in_array($file_extension, $audio_extensions)) {
                return "audio";
            } else {
                return "document";
            }       
        } else {
            throw new Exception("The message isn't of correct type");
        }
    }
}