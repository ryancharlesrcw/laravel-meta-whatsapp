# laravel-meta-whatsapp


## Getting started
Steps to install the laravel meta whatsapp package


### 1. Install the package
composer require ryancharleswijaya/laravel-meta-whatsapp


### 2. Publish The Config File
php artisan vendor:publish --provider="RyanCharlesWijaya\WhatsappServiceProvider"


### 3. Add Env Values
WHATSAPP_API_TOKEN=""
WHATSAPP_PHONE_NUMBER_ID=""